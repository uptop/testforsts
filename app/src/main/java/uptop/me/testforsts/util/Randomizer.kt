package uptop.me.testforsts.util

import java.util.*

object Randomizer {
    private val random = Random()
    private const val from: Int = 10
    private const val to: Int = 100

    fun rand() : Int {
        return random.nextInt(to - from) + from
    }
}