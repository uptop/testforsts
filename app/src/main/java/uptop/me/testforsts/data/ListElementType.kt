package uptop.me.testforsts.data

enum class ListElementType(val type: Int) {
    EVENT(0),
    MOVE(1),
    NOTICE(2)
}