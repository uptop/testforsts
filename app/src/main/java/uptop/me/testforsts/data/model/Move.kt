package uptop.me.testforsts.data.model

import uptop.me.testforsts.data.ListElement
import uptop.me.testforsts.data.ListElementType
import java.util.*

data class Move(var fromPlace: String?,var toPlace: String?,var estimateTime: Date?): ListElement {
    override fun getType(): Int {
        return ListElementType.MOVE.type
    }
}