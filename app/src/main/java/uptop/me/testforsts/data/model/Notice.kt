package uptop.me.testforsts.data.model

import uptop.me.testforsts.data.ListElement
import uptop.me.testforsts.data.ListElementType
import java.util.*

data class Notice(val flightDate: Date?, val gate: String?): ListElement {
    override fun getType(): Int {
        return ListElementType.NOTICE.type
    }
}