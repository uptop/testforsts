package uptop.me.testforsts.data.model

import uptop.me.testforsts.data.ListElement
import uptop.me.testforsts.data.ListElementType
import java.util.*

data class Event(var startTime: Date?, var endTime: Date?, var name: String?): ListElement {
    override fun getType(): Int {
        return ListElementType.EVENT.type
    }
}