package uptop.me.testforsts.data

interface ListElement {
    fun getType(): Int
}