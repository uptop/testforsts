package uptop.me.testforsts.model

import uptop.me.testforsts.data.ListElement
import uptop.me.testforsts.data.model.Event
import uptop.me.testforsts.data.model.Move
import uptop.me.testforsts.data.model.Notice
import uptop.me.testforsts.util.Randomizer.rand
import java.util.*
import kotlin.collections.ArrayList

class ListModel {
    lateinit var list: ArrayList<ListElement>
    var sizeList: Int = 0
    var listElements: ArrayList<ListElement> = ArrayList()

    fun getElements(): ArrayList<ListElement> {
        prepareBaseList()
        return makeList()
    }

    private fun prepareBaseList() {
        listElements.add(Move("from","to", Date(1000)))
        listElements.add(Notice(Date(1000), "GATE"))
        listElements.add(Event(Date(1000), Date(2000), "My Name"))
    }

    private fun makeList(): ArrayList<ListElement> {
        sizeList = rand()
        list = ArrayList(sizeList)
        for(i in 0..sizeList) {
            list.add(createRandomListElement())
        }

        return list
    }

    private fun createRandomListElement(): ListElement {
        return listElements.random()!!
    }

    private fun <E> List<E>.random(): E? = get(Random().nextInt(size))
}